package com.kmeraky.IoC;

public class DirectorEmpleado implements Empleados {

	//Creacion de campo tipo CreacionInforme (Interfaz)
	private CreacionInformes informeNuevo;
	private String email;
	private String nombreEmpresa;
	
	
	//Creacion del constructor que inyecta la dependencia.
	public DirectorEmpleado(CreacionInformes informeNuevo) {

		this.informeNuevo = informeNuevo;
	}
	
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestionar la plantilla de la empresa";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe creado por el Director: "+ informeNuevo.getInforme();
	}
	//como crear el meétodo init; Ejecutar tareas antes de que el bean este disponible
	
	public void metodoInicial() {
		System.out.println("Dentro del método init, Aqui van las tareas antes de que el bean este listo ");
	}
	
	//método destroy. Ejecutar tareas despues de que el bean hayasido utilizada.
	public void metodoDestoy() {
		System.out.println("Dentro del destroy, Aqui van las tareas despues de utilizar el bean");
	}


}
