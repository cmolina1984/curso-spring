package com.kmeraky.IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleado {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//creacion de objetos tipo empleado
		
		/*Empleados empleado1 = new JefeEmpleado();
		
		Empleados empleado2 = new SecretarioEmpleado();
		
		Empleados empleado3 = new DirectorEmpleado();
		
		System.out.println("Jefe: "+ empleado1.getTareas());
		
		System.out.println("Secretario: "+ empleado2.getTareas());
		
		System.out.println("Director: "+ empleado3.getTareas());*/
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		DirectorEmpleado Juan = context.getBean("miEmpleado",DirectorEmpleado.class);
		System.out.println("Inicio Datos del Director ");
		System.out.println(Juan.getTareas());
		System.out.println(Juan.getInforme());
		System.out.println("Los correo llegan a: "+ Juan.getEmail());
		System.out.println("De la empresa: "+Juan.getNombreEmpresa());
		
		SecretarioEmpleado Maria = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		System.out.println("................................");
		System.out.println("................................");
		System.out.println("Inicio Datos del Secretario ");
		System.out.println(Maria.getTareas());
		System.out.println(Maria.getInforme());
		System.out.println("Los correo llegan a: "+ Maria.getEmail());
		System.out.println("De la empresa: "+Maria.getNombreEmpresa());
		
		context.close();
		

	}

}
