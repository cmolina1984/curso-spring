package com.kmeraky.IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoDemoSingletonPrototype {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Carga de XML de configuración
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext2.xml");
		
		//petición de beans al contenedor Spring
		
		SecretarioEmpleado Maria = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		SecretarioEmpleado Pedro = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		SecretarioEmpleado Manolo = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);

		SecretarioEmpleado Juan = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);

		SecretarioEmpleado Ana = context.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);

		
		System.out.println(Maria);
		System.out.println(Pedro);
		System.out.println(Manolo);
		System.out.println(Juan);
		System.out.println(Ana);
		
		
		if(Maria==Pedro) {
			System.out.println("Apunta al mismo objeto");
		}else {
			System.out.println("No se trata del mismo objeto.");
		}


		context.close();


	}

}
