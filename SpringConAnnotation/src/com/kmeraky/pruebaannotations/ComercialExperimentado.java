package com.kmeraky.pruebaannotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Component("ComercialExperimentado")
@Component
//@Scope("prototype") no se puede trabajar con Posconstructor y Predestroy con prototype
public class ComercialExperimentado implements Empleados{
	
	@Autowired
	@Qualifier("informeFinancieroTrim2")//bean Id que debe utilizar 
	private CreacionInformeFinanciero nuevoInforme; //Incluso con un campo de clase.-  Usa el concepto Reflexión
	
	
	public ComercialExperimentado() {}
	
	/*
	@Autowired
	public ComercialExperimentado(CreacionInformeFinanciero nuevoInforme) {	
		this.nuevoInforme = nuevoInforme;
	}*/
	//Inyeccion con un émtodo setter
	
	/*@Autowired
	public void setNuevoInforme(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}*/
	
	//Inyeccion de dependencia con unmetodo cualquiera
	
	/*@Autowired
	public void nombreNoImporta(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}*/

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Vender Vender y Vender más !";
	}

	

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		//return "Informe gnerado por el comercial.";
		return nuevoInforme.getInformeFinanciero();
	}
	
	//Ejecución de código despues de creación del Bean
	
	@PostConstruct
	public void ejecutaDespuesCreación() {
		
		System.out.println("Ejecuta despues de creado el Bean");
		
	}
	//Ejecución de código despues de apagado contenedor Spring
	
	@PreDestroy
	public void ejecutaAntesDestruccion() {
		
		System.out.println("Ejecutando antes de la destruccion");
	}
	
}