package com.kmeraky.pruebaannotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// 1. Leer o cargar el XML de configuración 
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		
		//2. pedir un bean al contenedor
		
		Empleados Antonion= context.getBean("comercialExperimentado",Empleados.class);
		
		//3. usar el bean 
		
		System.out.println(Antonion.getInforme());
		System.out.println(Antonion.getTareas());
		
		//4. cerrar el contexto
		
		context.close();
		/* ojo esto es con objetos
		ComercialExperimentado com1 = new ComercialExperimentado();
		System.out.println("Inicio con objetos:  "+ com1.getInforme() +"--"+ com1.getTareas());
		*/


	}

}
