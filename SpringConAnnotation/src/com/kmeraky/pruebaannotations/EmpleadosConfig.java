package com.kmeraky.pruebaannotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.kmeraky.pruebaannotations")
@PropertySource("classpath:datosEmpresa.propiedades") //como alernativa no se puede poner classpath
public class EmpleadosConfig {

	// 1. definir el Bean para INforme finaciero DTOCOmpras

	@Bean
	public CreacionInformeFinanciero informeFinancieroDtoCompras() { // sera el id del Bean inyectado

		return new InformeFinancieroDtoCompras();

	}

	// 2. definir el bean para DirectorFinanciero e inyectar dependencia.

	@Bean
	public Empleados directorFinanciero() {
		return new DirectorFinanciero(informeFinancieroDtoCompras());

	}

}
