package com.kmeraky.pruebaannotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotation2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 1. Leer o cargar el XML de configuración
		//ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//leer el class de configuración
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(EmpleadosConfig.class);

		// 2. pedir un bean al contenedor
		//Empleados Antonion = context.getBean("comercialExperimentado", Empleados.class);
		
		//Empleados Lucia = context.getBean("comercialExperimentado", Empleados.class);
		
		//apuntan al mismo objeto en memoria
		
		/*if(Antonion==Lucia) {
			System.out.println("Apuntan al mismo lugar en memoria");
			System.out.println(Antonion +"\n" +Lucia);
		}else {
			
			System.out.println("No apuntan al mismo lugar en memoria");
			System.out.println(Antonion +"\n" +Lucia);
		}*/
		/*Empleados empleado=context.getBean("directorFinanciero", Empleados.class);
		
		System.out.println(empleado.getTareas());
		System.out.println(empleado.getInforme());

		*/
		DirectorFinanciero empleado =context.getBean("directorFinanciero",DirectorFinanciero.class);
		
		System.out.println("<==== INICIO DEL PROGRAMA ====>");
		

		
		System.out.println("Nombre de la Empresa: "+empleado.getNombreEmpresa());
		System.out.println("Correo Director Financiero:  " +empleado.getEmail());
		
		
		context.close();
		


	}

}
